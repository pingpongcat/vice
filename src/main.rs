use std::ffi::{c_void, CString};

use erupt::{
    utils::{self, surface},
    vk, EntryLoader,
};

use structopt::StructOpt;

use glfw::{Action, Context, Key};

mod instance;
use instance::*;

mod device;
use device::*;

mod buffer;
use buffer::*;

mod swapchain;
use swapchain::*;

mod pipeline;
use pipeline::*;

#[macro_export]
macro_rules! offset_of {
    ($base:path, $field:ident) => {{
        #[allow(unused_unsafe)]
        unsafe {
            let b: $base = std::mem::zeroed();
            (&b.$field as *const _ as isize) - (&b as *const _ as isize)
        }
    }};
}

const WINDOW_TITLE: &str = "Vice engine";
const FRAMES_IN_FLIGHT: usize = 2;
const SHADER_VERT: &[u8] = include_bytes!("triangle.vert.spv");
const SHADER_FRAG: &[u8] = include_bytes!("triangle.frag.spv");

struct Vertex {
    position: [f32; 2],
    color: [f32; 3],
}

const VERTICES: [Vertex; 4] = [
    Vertex {
        position: [-0.5, -0.5],
        color: [1.0, 0.0, 0.0],
    },
    Vertex {
        position: [0.5, -0.5],
        color: [0.0, 1.0, 0.0],
    },
    Vertex {
        position: [0.5, 0.5],
        color: [0.0, 0.0, 1.0],
    },
    Vertex {
        position: [-0.5, 0.5],
        color: [1.0, 1.0, 1.0],
    },
];

const INDICES: [u16; 6] = [0, 1, 2, 2, 3, 0];

fn main() {
    let options = Opt::from_args();

    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

    let (mut window, events) = glfw
        .create_window(1024, 768, WINDOW_TITLE, glfw::WindowMode::Windowed)
        .expect("Failed to create GLFW window.");

    let entry = EntryLoader::new().expect("Could not create instance");

    let mut instance = Instance::create(&options, &window, &entry);

    let surface = unsafe { surface::create_surface(&instance.loader, &window, None) }.unwrap();

    let mut device_extensions = vec![
        vk::KHR_SWAPCHAIN_EXTENSION_NAME,
    ];

    if options.use_rt {
        device_extensions.push(vk::KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
        device_extensions.push(vk::KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME);
        device_extensions.push(vk::KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME);
    }

    let device = Device::create(&mut instance, surface, device_extensions);
    let swapchain = Swapchain::create(&instance, surface, &device);

    let command_pool_info =
        vk::CommandPoolCreateInfoBuilder::new().queue_family_index(device.queue_family);

    let command_pool =
        unsafe { device.loader.create_command_pool(&command_pool_info, None) }.unwrap();

    let vertex_buffer = Buffer::create_vertex_buffer(
        &device,
        &instance,
        &command_pool,
        std::mem::size_of_val(&VERTICES) as u64,
        VERTICES.as_ptr() as *mut c_void,
    );

    let index_buffer = Buffer::create_index_buffer(
        &device,
        &instance,
        &command_pool,
        std::mem::size_of_val(&INDICES) as u64,
        INDICES.as_ptr() as *mut c_void,
    );
    //let buffer_adress = // AS
    /*
    let triangles = vk::AccelerationStructureGeometryTrianglesDataKHRBuilder::new()
        .vertex_format(vk::Format::R32G32B32_SFLOAT);
    */

    let entry_point = CString::new("main").unwrap();

    let vert_decoder = utils::decode_spv(SHADER_VERT).unwrap();
    let module_info = vk::ShaderModuleCreateInfoBuilder::new().code(&vert_decoder);
    let shader_vert = unsafe { device.loader.create_shader_module(&module_info, None) }.unwrap();

    let frag_decoder = utils::decode_spv(SHADER_FRAG).unwrap();
    let module_info = vk::ShaderModuleCreateInfoBuilder::new().code(&frag_decoder);
    let shader_frag = unsafe { device.loader.create_shader_module(&module_info, None) }.unwrap();

    let shader_stages = vec![
        vk::PipelineShaderStageCreateInfoBuilder::new()
            .stage(vk::ShaderStageFlagBits::VERTEX)
            .module(shader_vert)
            .name(&entry_point),
        vk::PipelineShaderStageCreateInfoBuilder::new()
            .stage(vk::ShaderStageFlagBits::FRAGMENT)
            .module(shader_frag)
            .name(&entry_point),
    ];

    let binding_description = [vk::VertexInputBindingDescriptionBuilder::new()
        .binding(0)
        .stride(std::mem::size_of::<Vertex>() as u32)
        .input_rate(vk::VertexInputRate::VERTEX)];

    let attribute_description = [
        vk::VertexInputAttributeDescriptionBuilder::new()
            .binding(0)
            .location(0)
            .format(vk::Format::R32G32_SFLOAT)
            .offset(offset_of!(Vertex, position) as u32),
        vk::VertexInputAttributeDescriptionBuilder::new()
            .binding(0)
            .location(1)
            .format(vk::Format::R32G32B32_SFLOAT)
            .offset(offset_of!(Vertex, color) as u32),
    ];

    let pipeline = Pipeline::create(
        &device,
        &swapchain,
        &binding_description,
        &attribute_description,
        shader_stages,
    );

    let frames: Vec<_> = swapchain
        .image_views
        .iter()
        .map(|image_view| {
            let attachments = vec![*image_view];

            let framebuffer_info = vk::FramebufferCreateInfoBuilder::new()
                .render_pass(pipeline.render_pass)
                .attachments(&attachments)
                .width(swapchain.surface_caps.current_extent.width)
                .height(swapchain.surface_caps.current_extent.height)
                .layers(1);

            unsafe { device.loader.create_framebuffer(&framebuffer_info, None) }.unwrap()
        })
        .collect();

    let cmd_buf_allocate_info = vk::CommandBufferAllocateInfoBuilder::new()
        .command_pool(command_pool)
        .level(vk::CommandBufferLevel::PRIMARY)
        .command_buffer_count(frames.len() as _);

    let cmd_bufs = unsafe {
        device
            .loader
            .allocate_command_buffers(&cmd_buf_allocate_info)
    }
    .unwrap();

    for (&cmd_buf, &framebuffer) in cmd_bufs.iter().zip(frames.iter()) {
        let cmd_buf_begin_info = vk::CommandBufferBeginInfoBuilder::new();

        unsafe {
            device
                .loader
                .begin_command_buffer(cmd_buf, &cmd_buf_begin_info)
        }
        .unwrap();

        let clear_values = vec![vk::ClearValue {
            color: vk::ClearColorValue {
                float32: [0.0, 0.0, 0.0, 1.0],
            },
        }];

        let render_pass_begin_info = vk::RenderPassBeginInfoBuilder::new()
            .render_pass(pipeline.render_pass)
            .framebuffer(framebuffer)
            .render_area(vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: swapchain.surface_caps.current_extent,
            })
            .clear_values(&clear_values);

        unsafe {
            device.loader.cmd_begin_render_pass(
                cmd_buf,
                &render_pass_begin_info,
                vk::SubpassContents::INLINE,
            );

            device
                .loader
                //TODO
                .cmd_bind_pipeline(cmd_buf, vk::PipelineBindPoint::GRAPHICS, pipeline.pipeline);

            let vertex_buffers = [vertex_buffer.buffer];

            device
                .loader
                .cmd_bind_vertex_buffers(cmd_buf, 0, &vertex_buffers, &[0]);

            device.loader.cmd_bind_index_buffer(
                cmd_buf,
                index_buffer.buffer,
                0,
                vk::IndexType::UINT16,
            );

            /*
            device
                .loader
                .cmd_draw(cmd_buf, VERTICES.len() as u32, 1, 0, 0);
            */
            device
                .loader
                .cmd_draw_indexed(cmd_buf, INDICES.len() as u32, 1, 0, 0, 0);

            device.loader.cmd_end_render_pass(cmd_buf);
            device.loader.end_command_buffer(cmd_buf).unwrap()
        }
    }

    let semaphore_info = vk::SemaphoreCreateInfoBuilder::new();

    let image_availible_semaphore: Vec<_> = (0..FRAMES_IN_FLIGHT)
        .map(|_| unsafe { device.loader.create_semaphore(&semaphore_info, None) }.unwrap())
        .collect();

    let render_finished_semaphores: Vec<_> = (0..FRAMES_IN_FLIGHT)
        .map(|_| unsafe { device.loader.create_semaphore(&semaphore_info, None) }.unwrap())
        .collect();

    let fence_info = vk::FenceCreateInfoBuilder::new().flags(vk::FenceCreateFlags::SIGNALED);

    let in_flight_fences: Vec<_> = (0..FRAMES_IN_FLIGHT)
        .map(|_| unsafe { device.loader.create_fence(&fence_info, None) }.unwrap())
        .collect();

    let mut images_in_flight: Vec<_> = swapchain
        .image_views
        .iter()
        .map(|_| vk::Fence::null())
        .collect();

    let mut frame = 0;

    window.set_key_polling(true);
    window.make_current();

    while !window.should_close() {
        glfw.poll_events();
        for (_, event) in glfw::flush_messages(&events) {
            handle_window_event(&mut window, event);
        }

        unsafe {
            device
                .loader
                .wait_for_fences(&[in_flight_fences[frame]], true, u64::MAX)
                .unwrap();
        }

        let image_index = unsafe {
            device.loader.acquire_next_image_khr(
                //TODO
                swapchain.swapchain,
                u64::MAX,
                image_availible_semaphore[frame],
                vk::Fence::null(),
            )
        }
        .unwrap();

        let image_in_flight = images_in_flight[image_index as usize];

        if !image_in_flight.is_null() {
            unsafe {
                device
                    .loader
                    .wait_for_fences(&[image_in_flight], true, u64::MAX)
            }
            .unwrap();
        }

        images_in_flight[image_index as usize] = in_flight_fences[frame];

        let wait_semaphores = vec![image_availible_semaphore[frame]];
        let command_buffers = vec![cmd_bufs[image_index as usize]];
        let signal_semaphores = vec![render_finished_semaphores[frame]];

        let submit_info = vk::SubmitInfoBuilder::new()
            .wait_semaphores(&wait_semaphores)
            .wait_dst_stage_mask(&[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
            .command_buffers(&command_buffers)
            .signal_semaphores(&signal_semaphores);

        unsafe {
            let in_flight_fence = in_flight_fences[frame];

            device.loader.reset_fences(&[in_flight_fence]).unwrap();
            device
                .loader
                .queue_submit(device.queue, &[submit_info], in_flight_fence)
                .unwrap();
        }
        //TODO
        let swapchains = vec![swapchain.swapchain];
        let image_indices = vec![image_index];
        let present_info = vk::PresentInfoKHRBuilder::new()
            .wait_semaphores(&signal_semaphores)
            .swapchains(&swapchains)
            .image_indices(&image_indices);

        //TODO
        unsafe { device.loader.queue_present_khr(device.queue, &present_info) }.unwrap();

        frame = (frame + 1) % FRAMES_IN_FLIGHT;
    }

    unsafe {
        device.loader.device_wait_idle().unwrap();

        for &semaphore in image_availible_semaphore
            .iter()
            .chain(render_finished_semaphores.iter())
        {
            device.loader.destroy_semaphore(semaphore, None);
        }

        for &fence in &in_flight_fences {
            device.loader.destroy_fence(fence, None);
        }

        device.loader.destroy_command_pool(command_pool, None);

        for &framebuffer in &frames {
            device.loader.destroy_framebuffer(framebuffer, None);
        }

        pipeline.destroy(&device);

        device.loader.destroy_shader_module(shader_vert, None);
        device.loader.destroy_shader_module(shader_frag, None);

        swapchain.destroy(&device);
        vertex_buffer.destoy(&device);
        index_buffer.destoy(&device);
        device.destroy();

        instance.loader.destroy_surface_khr(surface, None);

        instance.destoy();
    }
}

fn handle_window_event(window: &mut glfw::Window, event: glfw::WindowEvent) {
    match event {
        glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => window.set_should_close(true),
        _ => {}
    }
}
