use std::{
    ffi::{c_void, CStr, CString},
    os::raw::c_char,
};

use glfw;

use erupt::{cstr, utils::surface, vk, InstanceLoader};

use structopt::StructOpt;

const KHRONOS_VALIDATION_LAYER: *const c_char = cstr!("VK_LAYER_KHRONOS_validation");

#[derive(Debug, StructOpt)]
pub struct Opt {
    #[structopt(short, long)]
    pub validation_layers: bool,
    #[structopt(short, long)]
    pub use_rt: bool,
}

pub struct Instance {
    pub loader: InstanceLoader,
    pub validation_layers: Vec<*const i8>,
    pub messenger: vk::DebugUtilsMessengerEXT,
}

unsafe extern "system" fn debug_callback(
    _message_severity: vk::DebugUtilsMessageSeverityFlagBitsEXT,
    _message_types: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> vk::Bool32 {
    eprintln!(
        "{}",
        CStr::from_ptr((*p_callback_data).p_message).to_string_lossy()
    );

    vk::FALSE
}

impl Instance {
    pub fn create<T>(
        options: &Opt,
        window: &glfw::Window,
        entry: &erupt::CustomEntryLoader<T>,
    ) -> Self {
        let app_name = CString::new("Hello triangle").unwrap();
        let engine_name = CString::new("Vice").unwrap();

        let app_info = vk::ApplicationInfoBuilder::new()
            .application_name(&app_name)
            .application_version(vk::make_api_version(0, 1, 0, 0))
            .engine_name(&engine_name)
            .engine_version(vk::make_api_version(0, 1, 0, 0))
            .api_version(vk::API_VERSION_1_2);

        let mut instance_extensions = surface::enumerate_required_extensions(window).unwrap();

        if options.validation_layers {
            instance_extensions.push(vk::EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        let mut validation_layers = Vec::new();

        if options.validation_layers {
            validation_layers.push(KHRONOS_VALIDATION_LAYER);
        }

        let instance_info = vk::InstanceCreateInfoBuilder::new()
            .application_info(&app_info)
            .enabled_extension_names(&instance_extensions)
            .enabled_layer_names(&validation_layers);

        let loader = unsafe { InstanceLoader::new(&entry, &instance_info) }.unwrap();

        let messenger = if options.validation_layers {
            let messenger_info = vk::DebugUtilsMessengerCreateInfoEXTBuilder::new()
                .message_severity(
                    vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE_EXT
                        | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING_EXT
                        | vk::DebugUtilsMessageSeverityFlagsEXT::ERROR_EXT,
                )
                .message_type(
                    vk::DebugUtilsMessageTypeFlagsEXT::GENERAL_EXT
                        | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION_EXT
                        | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE_EXT,
                )
                .pfn_user_callback(Some(debug_callback));

            unsafe { loader.create_debug_utils_messenger_ext(&messenger_info, None) }.unwrap()
        } else {
            Default::default()
        };

        Instance {
            loader,
            validation_layers,
            messenger,
        }
    }

    pub fn destoy(&self) {
        unsafe {
            if !self.messenger.is_null() {
                self.loader
                    .destroy_debug_utils_messenger_ext(self.messenger, None);
            }

            self.loader.destroy_instance(None);
            println!("Exited cleanly");
        }
    }
}
