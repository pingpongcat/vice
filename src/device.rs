use std::ffi::CStr;

use erupt::{vk, DeviceLoader, ExtendableFrom};

use crate::instance::Instance;

pub struct Device {
    pub loader: DeviceLoader,
    pub physical_device: vk::PhysicalDevice,
    pub queue_family: u32,
    pub format: vk::SurfaceFormatKHR,
    pub present_mode: vk::PresentModeKHR,
    pub queue: vk::Queue,
}

impl Device {
    pub fn create(
        instance: &mut Instance,
        surface: vk::SurfaceKHR,
        device_extensions: Vec<*const i8>,
    ) -> Self {
        let (
            physical_device,
            queue_family,
            format,
            present_mode,
            device_properties,
            rt_pipeline_properties,
        ) = unsafe {
            instance
                .loader
                .enumerate_physical_devices(None)
                .unwrap()
                .into_iter()
                .filter_map(|physical_device| {
                    let queue_family = match instance
                        .loader
                        .get_physical_device_queue_family_properties(physical_device, None)
                        .into_iter()
                        .enumerate()
                        .position(|(i, queue_family_properties)| {
                            queue_family_properties
                                .queue_flags
                                .contains(vk::QueueFlags::GRAPHICS)
                                && instance
                                    .loader
                                    .get_physical_device_surface_support_khr(
                                        physical_device,
                                        i as u32,
                                        surface,
                                    )
                                    .unwrap()
                        }) {
                        Some(queue_family) => queue_family as u32,
                        None => return None,
                    };

                    let formats = instance
                        .loader
                        .get_physical_device_surface_formats_khr(physical_device, surface, None)
                        .unwrap();

                    let format = match formats
                        .iter()
                        .find(|surface_format| {
                            surface_format.format == vk::Format::B8G8R8A8_SRGB
                                && surface_format.color_space
                                    == vk::ColorSpaceKHR::SRGB_NONLINEAR_KHR
                        })
                        .or_else(|| formats.get(0))
                    {
                        Some(surface_format) => *surface_format,
                        None => return None,
                    };

                    let present_mode = instance
                        .loader
                        .get_physical_device_surface_present_modes_khr(
                            physical_device,
                            surface,
                            None,
                        )
                        .unwrap()
                        .into_iter()
                        .find(|present_mode| *present_mode == vk::PresentModeKHR::MAILBOX_KHR)
                        .unwrap_or(vk::PresentModeKHR::FIFO_KHR);

                    let supported_device_extensions = instance
                        .loader
                        .enumerate_device_extension_properties(physical_device, None, None)
                        .unwrap();

                    let device_extensions_supported =
                        device_extensions.iter().all(|device_extension| {
                            let device_extension = CStr::from_ptr(*device_extension);

                            supported_device_extensions.iter().any(|properties| {
                                CStr::from_ptr(properties.extension_name.as_ptr())
                                    == device_extension
                            })
                        });

                    if !device_extensions_supported {
                        return None;
                    }

                    let mut rt_pipeline_properties =
                        vk::PhysicalDeviceRayTracingPipelinePropertiesKHRBuilder::new()
                            .build_dangling();

                    let device_properties2 = vk::PhysicalDeviceProperties2Builder::new()
                        .extend_from(&mut rt_pipeline_properties)
                        .build_dangling();

                    let device_properties = instance
                        .loader
                        .get_physical_device_properties2(physical_device, Some(device_properties2));

                    Some((
                        physical_device,
                        queue_family,
                        format,
                        present_mode,
                        device_properties,
                        rt_pipeline_properties,
                    ))
                })
                .max_by_key(
                    |(_, _, _, _, properties, _)| match properties.properties.device_type {
                        vk::PhysicalDeviceType::DISCRETE_GPU => 2,
                        vk::PhysicalDeviceType::INTEGRATED_GPU => 1,
                        _ => 0,
                    },
                )
                .expect("No sutable device")
        };

        println!("Using physical device: {:?}", unsafe {
            CStr::from_ptr(device_properties.properties.device_name.as_ptr())
        });

        println!(
            "Max ray depth recursion: {}",
            rt_pipeline_properties.max_ray_recursion_depth
        );

        let queu_info = vec![vk::DeviceQueueCreateInfoBuilder::new()
            .queue_family_index(queue_family)
            .queue_priorities(&[1.0])];

        let mut rt_pipeline_feature = vk::PhysicalDeviceRayTracingPipelineFeaturesKHRBuilder::new();
        let mut as_feature = vk::PhysicalDeviceAccelerationStructureFeaturesKHRBuilder::new();
        let mut features12 = vk::PhysicalDeviceVulkan12FeaturesBuilder::new();
        let mut features11 = vk::PhysicalDeviceVulkan11FeaturesBuilder::new();

        let mut features2 = vk::PhysicalDeviceFeatures2Builder::new()
            .extend_from(&mut features12)
            .extend_from(&mut features11)
            .extend_from(&mut as_feature)
            .extend_from(&mut rt_pipeline_feature)
            .build_dangling();

        unsafe {
            instance
                .loader
                .get_physical_device_features2(physical_device, Some(features2));
        }

        let device_info = vk::DeviceCreateInfoBuilder::new()
            .queue_create_infos(&queu_info)
            .enabled_extension_names(&device_extensions)
            .enabled_layer_names(&mut instance.validation_layers)
            .extend_from(&mut features2);

        let loader =
            unsafe { DeviceLoader::new(&instance.loader, physical_device, &device_info) }.unwrap();
        let queue = unsafe { loader.get_device_queue(queue_family, 0) };
        Device {
            loader,
            physical_device,
            queue_family,
            format,
            present_mode,
            queue,
        }
    }

    pub fn destroy(&self) {
        unsafe {
            self.loader.destroy_device(None);
        }
    }
}
