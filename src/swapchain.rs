use erupt::vk;

use crate::device::Device;
use crate::instance::Instance;

pub struct Swapchain {
    pub swapchain: vk::SwapchainKHR,
    pub image_views: Vec<vk::ImageView>,
    pub surface_caps: vk::SurfaceCapabilitiesKHR,
}

impl Swapchain {
    pub fn create(instance: &Instance, surface: vk::SurfaceKHR, device: &Device) -> Swapchain {
        let surface_caps = unsafe {
            instance
                .loader
                .get_physical_device_surface_capabilities_khr(device.physical_device, surface)
        }
        .unwrap();

        let mut image_count = surface_caps.min_image_count + 1;
        if surface_caps.max_image_count > 0 && image_count > surface_caps.max_image_count {
            image_count = surface_caps.max_image_count;
        }

        let swapchain_info = vk::SwapchainCreateInfoKHRBuilder::new()
            .surface(surface)
            .min_image_count(image_count)
            .image_format(device.format.format)
            .image_color_space(device.format.color_space)
            .image_extent(surface_caps.current_extent)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .pre_transform(surface_caps.current_transform)
            .composite_alpha(vk::CompositeAlphaFlagBitsKHR::OPAQUE_KHR)
            .present_mode(device.present_mode)
            .clipped(true)
            .old_swapchain(vk::SwapchainKHR::null());

        let swapchain =
            unsafe { device.loader.create_swapchain_khr(&swapchain_info, None) }.unwrap();

        let images = unsafe { device.loader.get_swapchain_images_khr(swapchain, None) }.unwrap();

        let image_views: Vec<vk::ImageView> = images
            .iter()
            .map(|swapchain_image| {
                let image_view_info = vk::ImageViewCreateInfoBuilder::new()
                    .image(*swapchain_image)
                    .view_type(vk::ImageViewType::_2D)
                    .format(device.format.format)
                    .components(vk::ComponentMapping {
                        r: vk::ComponentSwizzle::IDENTITY,
                        g: vk::ComponentSwizzle::IDENTITY,
                        b: vk::ComponentSwizzle::IDENTITY,
                        a: vk::ComponentSwizzle::IDENTITY,
                    })
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    });
                unsafe { device.loader.create_image_view(&image_view_info, None) }.unwrap()
            })
            .collect();

        Swapchain {
            swapchain,
            image_views,
            surface_caps,
        }
    }

    pub fn destroy(&self, device: &Device) {
        unsafe {
            for &image_view in &self.image_views {
                device.loader.destroy_image_view(image_view, None);
            }
            device.loader.destroy_swapchain_khr(self.swapchain, None);
        }
    }
}
