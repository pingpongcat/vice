use std::ffi::c_void;

use erupt::vk;

use crate::device::Device;
use crate::instance::Instance;

pub struct Buffer {
    pub buffer: vk::Buffer,
}

fn create_buffer_and_memory(
    device: &Device,
    instance: &Instance,
    size: vk::DeviceSize,
    usage: vk::BufferUsageFlags,
    properties: vk::MemoryPropertyFlags,
) -> (vk::Buffer, vk::DeviceMemory) {
    let buffer_info = vk::BufferCreateInfoBuilder::new()
        .size(size)
        .usage(usage)
        .sharing_mode(vk::SharingMode::EXCLUSIVE);

    let buffer = unsafe { device.loader.create_buffer(&buffer_info, None) }
        .expect("Could not create buffer");

    let memory_requirements = unsafe { device.loader.get_buffer_memory_requirements(buffer) };

    let memory_properties = unsafe {
        instance
            .loader
            .get_physical_device_memory_properties(device.physical_device)
    };

    let (memory_type_index, _) = memory_properties
        .memory_types
        .into_iter()
        .enumerate()
        .find(|(_i, mt)| mt.property_flags == (properties))
        .expect("Could not find sutable memory type index");

    let alloc_info = vk::MemoryAllocateInfoBuilder::new()
        .allocation_size(memory_requirements.size)
        .memory_type_index(memory_type_index as u32);

    let buffer_memory = unsafe { device.loader.allocate_memory(&alloc_info, None) }.unwrap();

    unsafe { device.loader.bind_buffer_memory(buffer, buffer_memory, 0) }.unwrap();

    (buffer, buffer_memory)
}

fn copy_buffer(
    device: &Device,
    command_pool: &vk::CommandPool,
    src_buffer: vk::Buffer,
    dst_buffer: vk::Buffer,
    size: vk::DeviceSize,
) {
    let cmd_buf_allocate_info = vk::CommandBufferAllocateInfoBuilder::new()
        .command_pool(*command_pool)
        .level(vk::CommandBufferLevel::PRIMARY)
        .command_buffer_count(1);

    let cmd_bufs = unsafe {
        device
            .loader
            .allocate_command_buffers(&cmd_buf_allocate_info)
    }
    .unwrap();

    let cmd_buf_begin_info = vk::CommandBufferBeginInfoBuilder::new()
        .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

    let copy_regions = [vk::BufferCopyBuilder::new().size(size)];

    unsafe {
        let _ = device
            .loader
            .begin_command_buffer(cmd_bufs[0], &cmd_buf_begin_info);

        device
            .loader
            .cmd_copy_buffer(cmd_bufs[0], src_buffer, dst_buffer, &copy_regions);

        device.loader.end_command_buffer(cmd_bufs[0]).unwrap()
    }

    let submit_info = vk::SubmitInfoBuilder::new().command_buffers(&cmd_bufs);

    unsafe {
        device
            .loader
            .queue_submit(device.queue, &[submit_info], vk::Fence::null())
            .unwrap();

        device.loader.device_wait_idle().unwrap();

        device.loader.free_command_buffers(*command_pool, &cmd_bufs);
    }
}

impl Buffer {
    pub fn create_vertex_buffer(
        device: &Device,
        instance: &Instance,
        command_pool: &vk::CommandPool,
        size: u64,
        ptr: *mut c_void,
    ) -> Buffer {
        let (buffer, buffer_memory) = create_buffer_and_memory(
            device,
            instance,
            size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
        );

        let data = unsafe {
            device
                .loader
                .map_memory(buffer_memory, 0, size, vk::MemoryMapFlags::default())
        }
        .unwrap();

        unsafe { std::ptr::copy(ptr, data, size as usize) };

        unsafe { device.loader.unmap_memory(buffer_memory) }

        let (vertex_buffer, _) = create_buffer_and_memory(
            device,
            instance,
            size,
            vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::VERTEX_BUFFER,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        );

        copy_buffer(device, command_pool, buffer, vertex_buffer, size);

        unsafe {
            device.loader.destroy_buffer(buffer, None);
            device.loader.free_memory(buffer_memory, None);
        }

        Buffer {
            buffer: vertex_buffer,
        }
    }

    //TODO code duplication
    pub fn create_index_buffer(
        device: &Device,
        instance: &Instance,
        command_pool: &vk::CommandPool,
        size: u64,
        ptr: *mut c_void,
    ) -> Buffer {
        let (buffer, buffer_memory) = create_buffer_and_memory(
            device,
            instance,
            size,
            vk::BufferUsageFlags::TRANSFER_SRC,
            vk::MemoryPropertyFlags::HOST_VISIBLE | vk::MemoryPropertyFlags::HOST_COHERENT,
        );

        let data = unsafe {
            device
                .loader
                .map_memory(buffer_memory, 0, size, vk::MemoryMapFlags::default())
        }
        .unwrap();

        unsafe { std::ptr::copy(ptr, data, size as usize) };

        unsafe { device.loader.unmap_memory(buffer_memory) }

        let (vertex_buffer, _) = create_buffer_and_memory(
            device,
            instance,
            size,
            vk::BufferUsageFlags::TRANSFER_DST | vk::BufferUsageFlags::INDEX_BUFFER,
            vk::MemoryPropertyFlags::DEVICE_LOCAL,
        );

        copy_buffer(device, command_pool, buffer, vertex_buffer, size);

        unsafe {
            device.loader.destroy_buffer(buffer, None);
            device.loader.free_memory(buffer_memory, None);
        }

        Buffer {
            buffer: vertex_buffer,
        }
    }

    pub fn destoy(&self, device: &Device) {
        unsafe {
            device.loader.destroy_buffer(self.buffer, None);
        }
    }
}
