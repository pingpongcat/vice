use erupt::vk;

use crate::device::Device;
use crate::swapchain::Swapchain;

pub struct Pipeline {
    pub pipeline: vk::Pipeline,
    pub render_pass: vk::RenderPass,
    pub pipeline_layout: vk::PipelineLayout,
}

impl Pipeline {
    pub fn create(
        device: &Device,
        swapchain: &Swapchain,
        binding_description: &[vk::VertexInputBindingDescriptionBuilder],
        attribute_description: &[vk::VertexInputAttributeDescriptionBuilder],
        shader_stages: Vec<vk::PipelineShaderStageCreateInfoBuilder>,
    ) -> Pipeline {
        // OLD
        //let vertex_input = vk::PipelineVertexInputStateCreateInfoBuilder::new();
        let vertex_input = vk::PipelineVertexInputStateCreateInfoBuilder::new()
            .vertex_binding_descriptions(binding_description)
            .vertex_attribute_descriptions(attribute_description);

        let input_assembly = vk::PipelineInputAssemblyStateCreateInfoBuilder::new()
            .topology(vk::PrimitiveTopology::TRIANGLE_LIST)
            .primitive_restart_enable(false);

        let viewports = vec![vk::ViewportBuilder::new()
            .x(0.0)
            .y(0.0)
            .width(swapchain.surface_caps.current_extent.width as f32)
            .height(swapchain.surface_caps.current_extent.height as f32)
            .min_depth(0.0)
            .max_depth(1.0)];

        let scissors = vec![vk::Rect2DBuilder::new()
            .offset(vk::Offset2D { x: 0, y: 0 })
            .extent(swapchain.surface_caps.current_extent)];

        let viewport_state = vk::PipelineViewportStateCreateInfoBuilder::new()
            .viewports(&viewports)
            .scissors(&scissors);

        let rasterizer = vk::PipelineRasterizationStateCreateInfoBuilder::new()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .line_width(1.0)
            .cull_mode(vk::CullModeFlags::BACK)
            .front_face(vk::FrontFace::CLOCKWISE)
            .depth_clamp_enable(false);

        let multisampling = vk::PipelineMultisampleStateCreateInfoBuilder::new()
            .sample_shading_enable(false)
            .rasterization_samples(vk::SampleCountFlagBits::_1);

        let color_blend_attachments = vec![vk::PipelineColorBlendAttachmentStateBuilder::new()
            .color_write_mask(
                vk::ColorComponentFlags::R
                    | vk::ColorComponentFlags::G
                    | vk::ColorComponentFlags::B
                    | vk::ColorComponentFlags::A,
            )
            .blend_enable(false)];

        let color_blending = vk::PipelineColorBlendStateCreateInfoBuilder::new()
            .logic_op_enable(false)
            .attachments(&color_blend_attachments);

        let pipeline_layout_info = vk::PipelineLayoutCreateInfoBuilder::new();

        let pipeline_layout = unsafe {
            device
                .loader
                .create_pipeline_layout(&pipeline_layout_info, None)
        }
        .unwrap();

        let attachments = vec![vk::AttachmentDescriptionBuilder::new()
            .format(device.format.format)
            .samples(vk::SampleCountFlagBits::_1)
            .load_op(vk::AttachmentLoadOp::CLEAR)
            .store_op(vk::AttachmentStoreOp::STORE)
            .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .final_layout(vk::ImageLayout::PRESENT_SRC_KHR)];

        let color_attachment_refs = vec![vk::AttachmentReferenceBuilder::new()
            .attachment(0)
            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)];

        let subpass = vec![vk::SubpassDescriptionBuilder::new()
            .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
            .color_attachments(&color_attachment_refs)];

        let dependencies = vec![vk::SubpassDependencyBuilder::new()
            .src_subpass(vk::SUBPASS_EXTERNAL)
            .dst_subpass(0)
            .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .src_access_mask(vk::AccessFlags::empty())
            .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE)];

        let render_pass_info = vk::RenderPassCreateInfoBuilder::new()
            .attachments(&attachments)
            .subpasses(&subpass)
            .dependencies(&dependencies);

        let render_pass =
            unsafe { device.loader.create_render_pass(&render_pass_info, None) }.unwrap();

        let pipeline_info = vk::GraphicsPipelineCreateInfoBuilder::new()
            .stages(&shader_stages)
            .vertex_input_state(&vertex_input)
            .input_assembly_state(&input_assembly)
            .viewport_state(&viewport_state)
            .rasterization_state(&rasterizer)
            .multisample_state(&multisampling)
            .color_blend_state(&color_blending)
            .layout(pipeline_layout)
            .render_pass(render_pass)
            .subpass(0);

        let pipeline = unsafe {
            device.loader.create_graphics_pipelines(
                vk::PipelineCache::null(),
                &[pipeline_info],
                None,
            )
        }
        .unwrap()[0];

        Pipeline {
            pipeline,
            render_pass,
            pipeline_layout,
        }
    }

    pub fn destroy(&self, device: &Device) {
        unsafe {
            device.loader.destroy_pipeline(self.pipeline, None);

            device.loader.destroy_render_pass(self.render_pass, None);

            device
                .loader
                .destroy_pipeline_layout(self.pipeline_layout, None);
        }
    }
}
